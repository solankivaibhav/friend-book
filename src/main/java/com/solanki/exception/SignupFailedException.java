package com.solanki.exception;

public class SignupFailedException extends Exception {

	private static final long serialVersionUID = 1L;

	public SignupFailedException(String message) {
		super(message);
	}
}
