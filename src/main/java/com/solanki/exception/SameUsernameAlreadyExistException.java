package com.solanki.exception;

public class SameUsernameAlreadyExistException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SameUsernameAlreadyExistException(String message) {
		super(message);
	}

	
}
