package com.solanki.exception;

public class UserAlreadyInFriendListException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public UserAlreadyInFriendListException(String message) {
		super(message);
	}
}
