package com.solanki.exception;

public class UserAndFriendMustNotBeSameException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UserAndFriendMustNotBeSameException(String message) {
		super(message);
	}
	
}
