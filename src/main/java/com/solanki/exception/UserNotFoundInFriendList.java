package com.solanki.exception;

public class UserNotFoundInFriendList extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public UserNotFoundInFriendList(String message) {
		super(message);
	}
	
}
