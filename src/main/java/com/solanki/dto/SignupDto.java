package com.solanki.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class SignupDto {

	@NotBlank(message="first name must not blank or empty")
	private String firstName;

	@NotBlank(message="last name must not blank or empty")
	private String lastName;

	@NotBlank(message="user name must not blank or empty")
	private String username;

	@Size(min=6,message="password size must be greater than 6 char")
	private String password;

	@Email(message="email address is invalid")
	private String emailAddress;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}
