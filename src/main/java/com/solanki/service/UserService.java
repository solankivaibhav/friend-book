package com.solanki.service;

import java.util.List;

import com.solanki.dto.LoginDto;
import com.solanki.dto.UpdateUserDetailsDto;
import com.solanki.model.User;

public interface UserService {

	public void save(User user);

	public User getUserByUsername(String username);

	public User addFriendIntoFriendList(long userId, long friendId);

	public User loginUser(LoginDto loginDto);

	public User updateUser(UpdateUserDetailsDto updateUserDetailsDto);

	public User removeFriendFromFriendList(long userId, long friendId);

	public List<User> getAllUsers();

}
