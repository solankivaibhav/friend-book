package com.solanki.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.solanki.dto.LoginDto;
import com.solanki.dto.UpdateUserDetailsDto;
import com.solanki.exception.LoginFailedException;
import com.solanki.exception.SameUsernameAlreadyExistException;
import com.solanki.exception.UserAlreadyInFriendListException;
import com.solanki.exception.UserAndFriendMustNotBeSameException;
import com.solanki.exception.UserNotFoundException;
import com.solanki.exception.UserNotFoundInFriendList;
import com.solanki.model.User;
import com.solanki.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserRepository userRepository;
	
	public User getUserByUsername(String username) {
		User user = userRepository.findByUsername(username);
		if(user==null) {
			throw new UserNotFoundException("user not found");
		}else {
			return user;
		}
	}
	
	public void save(User user) {
		User user1 = userRepository.findByUsername(user.getUsername());
		if(user1==null) {
			userRepository.save(user);
		}else {
			throw new SameUsernameAlreadyExistException("same username already exist, please try other one");
		}
		
	}

	@Override
	public User addFriendIntoFriendList(long userId, long friendId) {
		
		if(userId==friendId) {
			throw new UserAndFriendMustNotBeSameException("user and friend must not be same");
		}
		
		Optional<User> optionalUser = userRepository.findById(userId);
		User user=null;
		if(optionalUser.isPresent()) {
			user= optionalUser.get();
		}else {
			throw new UserNotFoundException("user not found");
		}
		
		Optional<User> optionalFriend = userRepository.findById(friendId);
		User friend=null;
		if(optionalFriend.isPresent()) {
			friend= optionalFriend.get();
		}else {
			throw new UserNotFoundException("friend not found");
		}
		
		Set<User> friends = user.getFriends();
		for(User friend1:friends) {
			if(friend1.getId()==friend.getId()) {
				throw new UserAlreadyInFriendListException("user is already in friend list");
			}
		}
		friends.add(friend);
		
		user.setFriends(friends);
		
		userRepository.save(user);
		
		return user;
	}

	@Override
	public User loginUser(LoginDto loginDto) {

		User user = getUserByUsername(loginDto.getUsername());
		int diff = user.getPassword().compareTo(loginDto.getPassword());
		if(diff==0) {
			return user;
		}else {
			throw new LoginFailedException("login failed due to password is incorrect");
		}
	}

	@Override
	public User updateUser(UpdateUserDetailsDto updateUserDetailsDto) {
		
		Optional<User> optionalUser = userRepository.findById(updateUserDetailsDto.getId());
		if(!optionalUser.isPresent()) {
			throw new UserNotFoundException("user not found");
		}
		ModelMapper modelMapper = new ModelMapper();
		User user = modelMapper.map(updateUserDetailsDto, User.class);
		
		return userRepository.saveAndFlush(user);
	}

	@Override
	public User removeFriendFromFriendList(long userId, long friendId) {
		if(userId==friendId) {
			throw new UserAndFriendMustNotBeSameException("user and friend must not be same");
		}
		
		Optional<User> optionalUser = userRepository.findById(userId);
		User user=null;
		if(optionalUser.isPresent()) {
			user= optionalUser.get();
		}else {
			throw new UserNotFoundException("user not found");
		}
		
		Optional<User> optionalFriend = userRepository.findById(friendId);
		User friend=null;
		if(optionalFriend.isPresent()) {
			friend= optionalFriend.get();
		}else {
			throw new UserNotFoundException("friend not found");
		}
		
		Set<User> friends = user.getFriends();
		boolean isUserRemovedFromFriendList = false;
		
		for(User friend1:friends) {
			if(friend1.getId()==friend.getId()) {
				friends.remove(friend);
				isUserRemovedFromFriendList=true;
				break;
			}
		}
		if(isUserRemovedFromFriendList==false) {
			throw new UserNotFoundInFriendList("user not found in friend list");
		}
		user.setFriends(friends);
		userRepository.save(user);
		return user;
	}

	@Override
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}
}
