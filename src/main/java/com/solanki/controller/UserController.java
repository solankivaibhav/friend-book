package com.solanki.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.solanki.dto.LoginDto;
import com.solanki.dto.SignupDto;
import com.solanki.dto.UpdateUserDetailsDto;
import com.solanki.exception.SignupFailedException;
import com.solanki.exception.UserNotFoundException;
import com.solanki.model.User;
import com.solanki.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	private UserService userServiceImpl;

	@SuppressWarnings({ "rawtypes" })
	@PostMapping("/signup")
	public ResponseEntity createUser(@Valid @RequestBody SignupDto signupDto, BindingResult result)
			throws SignupFailedException {
		Map<String, Object> errors = new HashMap<>();

		try {
			if (result.hasErrors()) {
				return bindErrorIntoErrorList(result, errors);
			} else {
				ModelMapper modelMapper = new ModelMapper();
				User user = modelMapper.map(signupDto, User.class);
				userServiceImpl.save(user);
				return new ResponseEntity<>(user, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			throw new SignupFailedException(e.getMessage());
		}
	}
	
	@PutMapping()
	public ResponseEntity updateUser(@RequestBody UpdateUserDetailsDto updateUserDetailsDto) {
		
		User user = userServiceImpl.updateUser(updateUserDetailsDto);
		
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@PostMapping("/login")
	public ResponseEntity<User> loginUser(@RequestBody LoginDto loginDto) {
	
		User user = userServiceImpl.loginUser(loginDto);
		
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@GetMapping("/{username}")
	public ResponseEntity<User> getUserbyUsername(@PathVariable("username") String username) {
		User user = null;
		try {
			user = userServiceImpl.getUserByUsername(username);
		} catch (UserNotFoundException e) {
			throw new UserNotFoundException(e.getMessage());
		}
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@PutMapping("{userId}/add/friend/{friendId}")
	public ResponseEntity<User> addFriendInfoFriendList(@PathVariable("userId") long userId, @PathVariable("friendId") long friendId) {
		User user= null;
		
		user = userServiceImpl.addFriendIntoFriendList(userId,friendId);
		
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	@PutMapping("{userId}/remove/friend/{friendId}")
	public ResponseEntity<User> removeFriendFromFriendList(@PathVariable("userId") long userId, @PathVariable("friendId") long friendId) {
		User user= null;
		
		user = userServiceImpl.removeFriendFromFriendList(userId,friendId);
		
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@GetMapping()
	public ResponseEntity<List<User>> getAllUsers() {
		
		List<User> users = userServiceImpl.getAllUsers();
		
		return new ResponseEntity<>(users, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ResponseEntity bindErrorIntoErrorList(BindingResult result, Map<String, Object> errors) {
		if (result.hasErrors()) {
			ArrayList<String> errorList = new ArrayList<String>();
			for (Object object : result.getAllErrors()) {
				if (object instanceof FieldError) {
					FieldError error = (FieldError) object;
					errorList.add(error.getDefaultMessage());
				}
			}
			errors.put("error", errorList);
			return new ResponseEntity(errors, HttpStatus.NOT_ACCEPTABLE);
		}
		return null;
	}

}