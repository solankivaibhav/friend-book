package com.solanki.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ControllerLoggingAspect {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Before("execution(* com.solanki.controller.*.*(..))")
	public void before(JoinPoint joinPoint) {

		logger.info("enter into controller method :: {}", joinPoint.getSignature().getName());
	}

	@After("execution(* com.solanki.controller.*.*(..))")
	public void after(JoinPoint joinPoint) {
		logger.info("exiting form controller method :: {}" ,joinPoint.getSignature().getName());
	}

	@AfterReturning(value = "execution(* com.solanki.controller.*.*(..))", returning = "result")
	public void afterReturning(JoinPoint joinPoint, Object result) {
		if (logger.isDebugEnabled()) {
			logger.info((String) result);
		}

		logger.info("{} returned with value {}", joinPoint, result);

	 }

}
