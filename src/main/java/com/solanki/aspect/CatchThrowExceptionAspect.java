package com.solanki.aspect;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.stereotype.Component;

@Aspect
@Component

public class CatchThrowExceptionAspect implements ThrowsAdvice {

	@AfterThrowing(pointcut = "execution(* com.peaas.*.*.*(..))", throwing = "ex")
	public void aferThrowing(Exception ex) {
		System.out.println("****************************************************");
		System.out.println("Inside CatchThrowException.afterThrowing() method...");
		System.out.println("Running after throwing exception........");
		System.out.println("Exception : " + ex);
		System.out.println("****************************************************");
	}
}
